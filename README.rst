CMD TO EXECUTE
==============

2 files needed to be edited BEFORE any operation here.


1- convert *env.example* to **.env**, fill all keys with data.

2- convert *config.example.py* to **config.py**, fill all obligatory informations.


Main execution script for all operation, including deployment, without any
virtualenv pre-load:

::

   $ python exec.py

::

-v --verbose            displays all executed steps
-N --no-deployment      avoid the deployment step part of the script

TODOS
^^^^^

-  re-think the deployment strategy, with tools like Ansible or Fabric
-  git the project for versioning, CI and deployment
