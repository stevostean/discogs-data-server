from datetime import datetime
from glob import glob
import math
import os, sys


sys.path.append("..")

import pandas as pd
from pandas.util.testing import assert_frame_equal

from app.utils.logger import LOGGER
from app.utils.files import DB, JSON_FILES, get_filename


def get_csv_file():
    os.chdir(DB)
    csv_file = glob("./*.csv")
    if len(csv_file):
        return csv_file[0]
    raise Exception("No CSV file at all.")


now = datetime.now()
today = now.strftime("%Y%m%d%H%M")


def cheap_batch(csv_file, mini=4):
    df = pd.read_csv(csv_file, na_filter=False)
    return df.loc[df["price"] < mini]


def remainder(csv_file, remain=4):
    df = pd.read_csv(csv_file, na_filter=False)
    return df.loc[df["price"] >= remain]


def all_marketplace(csv_file):
    return pd.read_csv(csv_file, na_filter=False)


def json_file(df, df_name, path=None):
    directory = JSON_FILES
    if not os.path.isdir(directory):
        os.makedirs(directory)

    json_files = os.listdir(os.path.join(directory, "."))

    for jfile in json_files:
        if jfile == df_name or jfile.startswith("marketplace"):
            pre_existant = os.path.join(directory, jfile)
            local_json = pd.read_json(pre_existant, orient="records")
            try:
                assert_frame_equal(df, local_json)
                return True
            except:
                os.remove(pre_existant)

    df.to_json(f"{directory}/{df_name}", orient="records")
    return True


def run():
    this_file = get_filename(__file__)
    csv_file = get_csv_file()
    batch_4_euros = cheap_batch(csv_file)
    batch_size = len(batch_4_euros)
    remains = remainder(csv_file)
    remains_size = len(remains)

    all_records = all_marketplace(csv_file)

    total = batch_size + remains_size
    batch_percent = math.floor(batch_size / total * 100)
    remains_percent = math.floor(remains_size / total * 100)

    LOGGER.info(f"[{this_file}] Low-Costs (< 4 euros): {batch_size}, {batch_percent} %")
    LOGGER.info(
        f"[{this_file}] Bankables (>= 4 euros): {remains_size}, {remains_percent} %"
    )

    json_file(batch_4_euros, "lowcosts.json", path="json_files")
    json_file(remains, f"bankables.json", path="json_files")
    json_file(all_records, f"marketplace_{today}.json", path="json_files")


if __name__ == "__main__":
    run()
