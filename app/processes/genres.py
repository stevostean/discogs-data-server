"""
PARALLEL HANDLING GENRES FROM RELEASES to create an API endpoint
"""
import concurrent.futures as CF
from itertools import chain
import json
import os, sys

sys.path.append("..")

from app.utils.logger import LOGGER
from app.utils.files import JSON_FILES, RELEASES, get_filename

CPUS = os.cpu_count()
collection = {}


def parseReleaseDir(as_list=True):
    """
    Get the list of JSON files in RELEASE path
    :param as_list: if False: tuple, if True: list
    :return: (list | tuple)
    """
    for _, _, files in os.walk(RELEASES):
        files_paths = map(lambda f: os.path.join(RELEASES, f), files)

    if as_list:
        return list(files_paths)
    return tuple(files_paths)


def ParseReleasesGenre():
    """
    Experimental function to get python dict from JSON files
    :return: (int, set)
    """

    list_of_obj = []
    full_paths = parseReleaseDir()

    for jsonpath in full_paths:
        with open(jsonpath) as jsonfile:
            list_of_obj.append(json.load(jsonfile)["genres"])

    list_of_obj = list(chain.from_iterable(list_of_obj))
    return len(list_of_obj), set(list_of_obj)


def genresHandler(release_file):
    """
    A simple function for handling genres in collection, via concurrent.futures (async parallelism)
    :param release_file: JSON filepath, for each release
    :return: True
    """

    global collection

    release_id = os.path.basename(release_file)  # extract filename + extension
    release_id, _ = os.path.splitext(
        release_id
    )  # get only the filename without extension

    with open(release_file) as rf:
        genres = json.load(rf)["genres"]

    for genre in genres:
        genre = genre.replace(" ", "").replace("'s", "").lower()
        keys = collection.keys()
        if genre not in keys:
            for k, v in collection.items():
                for entry in v:
                    if release_id == entry:
                        collection[k].remove(entry)
            collection[genre] = [release_id]
        else:
            collection[genre].append(release_id)
    return True


def write_json_genre_file(py_obj, out=False):
    """
    Write JSON file with all genres from releases
    :param py_obj: python dictionary
    :param out: if True, display new path with JSON file
    :return: void
    """

    this_script = get_filename(__file__)

    filepath = os.path.join(JSON_FILES, "genres.json")
    with open(filepath, "w") as f:
        f.write(json.dumps(py_obj, sort_keys=True, indent=2))

    if out:
        # print(f"Genres are written in {filepath}")
        LOGGER.info(f"[{this_script}] Genres are written in {filepath}")

    return


def async_data():
    """
    Parallel processing to get all data from JSON release files (better performance)
    :return: void
    """

    allReleases = parseReleaseDir()
    with CF.ThreadPoolExecutor(max_workers=CPUS) as executor:
        futures = {executor.submit(genresHandler, release) for release in allReleases}
        CF.wait(futures)


def run():
    async_data()
    write_json_genre_file(collection, out=True)


if __name__ == "__main__":
    run()
