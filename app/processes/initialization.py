"""
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

"""
from concurrent.futures import ThreadPoolExecutor, wait
from datetime import datetime
import glob
import os
import sys

sys.path.append("..")

import pandas as pd
from pandas.util.testing import assert_frame_equal


from app.utils.configuration import WORKERS as WK
from app.utils.files import check_or_create_directory, get_filename
from app.utils.logger import LOGGER
from app.processes.threaded_data import all_sales, marketplaceCollector

WORKERS = WK

SALES = all_sales()


def csv_filename(name="discogs_sales"):
    """

    :return: string
    """
    now = datetime.now()
    file_format_time = now.strftime("%Y%m%d-%H%M%S")
    return f"{name}_{file_format_time}.csv"


def run():
    global this_script, err, nu_created_path, size, nu_file_path
    this_script = get_filename(__file__)
    db_repo = check_or_create_directory("db")
    try:
        int_data = glob.glob(os.path.join(db_repo, "*.csv"))[0]
    except IndexError:
        int_data = None
    with ThreadPoolExecutor(max_workers=WORKERS) as executor:
        try:
            futures = [executor.submit(marketplaceCollector, sale) for sale in SALES]
            wait(futures)
        except TypeError as err:
            LOGGER.warning(f"Broken parallel request. Error: {err}")
            sys.exit(1)  # abnornal termination
    df2 = pd.DataFrame([item.result() for item in futures])
    if int_data is None:
        nu_created_filename = csv_filename()
        nu_created_path = os.path.join(db_repo, nu_created_filename)
        df2.to_csv(nu_created_path, index=False)

        LOGGER.info(f"[{this_script}] New DB file created: {nu_created_path}")
        sys.exit(0)
    df1 = pd.read_csv(int_data, na_filter=False)
    try:
        assert_frame_equal(df1, df2)
        size = len(df1)

        LOGGER.info(f"[{this_script}] No changes, {size} items.")
    except:
        os.remove(int_data)
        nu_filename = csv_filename()
        nu_file_path = os.path.join(db_repo, nu_filename)
        df2.to_csv(nu_file_path, index=False)

        LOGGER.warning(
            f"[{this_script}] New changes from external data, in {nu_file_path}"
        )


if __name__ == "__main__":
    run()
