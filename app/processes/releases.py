"""
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

This code helps to get each record from the marketplace in its own JSON file.
All are written in 'files/releases/' directory.
"""
import asyncio
from json import dump
from glob import glob
import os
from random import random
import sys
import threading
from time import time

sys.path.append("..")

import pandas as pd
import requests

from app.utils.logger import LOGGER, STREAM
from app.utils.discogs_requests import HEADERS
from app.utils.files import (
    RELEASES,
    JSON_FILES,
    check_or_create_directory,
    get_file_startswith,
    get_filename,
)

MARKETPLACE = JSON_FILES
# TODO: MARKETPLACE = JSON_FILES (other location)

releases = check_or_create_directory(RELEASES)
this_file = get_filename(__file__)
LOGGER.removeHandler(STREAM)  # no console logs PLEASE during async operations!
start = time()


async def data_task(res, seconds=0.1, progress=1):
    """
    Gets each requested link to get urlividual data.
    :param res: requests object
    :param seconds: integer|seconds
    :return: tuple|(dict, string)
    """
    try:
        assert res.status_code == 200

        data = res.json()
        await asyncio.sleep(seconds)

        elapsed = time() - start
        rel_ID = data["id"]
        file_name = f"{rel_ID}.json"

        prog = progress / (total_files - remain_files_in_releases) * 100
        for_logger = f"[{this_file}] job N#{rel_ID}, sleep: {seconds:.2f} sec, "
        for_logger += f"elapsed: {elapsed:.2f} sec, prog: {prog:.1f} %'"

        LOGGER.info(for_logger)

        return data, file_name
    except AssertionError:
        await asyncio.sleep(0.2)
        LOGGER.warning(f"[{this_file}] no data and status error {res.status_code}")


def write_release_file(df, file_name):
    """
    Writes a JSON file with its data (Synchronous, obsolete).
    :param df: dataframe|pandas
    :param file_name: string|filename
    :return: bool
    """
    abs_file = os.path.join(releases, file_name)

    with open(abs_file, "w") as outfile:
        dump(df, outfile, urlent=4)

    return True


async def awrite_release_file(df, file_name):
    """
    Writes a JSON file with its data, asynchronously.
    :param df: dataframe|pandas
    :param file_name: string|filename
    :return: bool
    """
    abs_file = os.path.join(releases, file_name)

    with open(abs_file, "w") as outfile:
        dump(df, outfile, indent=4)

    return abs_file


async def chained_funcs(url, count=1):
    """Chaining all asynchronous functions below in a one method.
    FROM: https://pymotw.com/3/asyncio/coroutines.html
    """
    df, fn = await data_task(requests.get(url, headers=HEADERS), progress=count)
    func2 = await awrite_release_file(df, fn)

    return func2


# Dynamic search for file.
json_marketplace = get_file_startswith(
    JSON_FILES,
    # start='discogs_sales',
    start="marketplace",
    full=True,
)
mp_pd = None
total_files = 0
if json_marketplace:
    mp_pd = pd.read_json(json_marketplace)
    total_files = len(mp_pd)
    remain_files_in_releases = len(glob(os.path.join(RELEASES, "*.json")))
    main_json_file = json_marketplace.split("/")[-1].replace(".json", "")
    LOGGER.info(f"[{this_file}] {main_json_file} was readed.")


def thr(url):
    """we need to create a new loop for the thread, and set it as the 'default'
    loop that will be returned by calls to asyncio.get_event_loop() from this
    thread.
    FROM: https://gist.github.com/lars-tiede/01e5f5a551f29a5f300e
    """
    thr.c += 1
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    loop.run_until_complete(chained_funcs(url, count=thr.c))
    loop.close()


thr.c = 0


def run():
    """Threading each release to get a single JSON in files/releases."""
    if mp_pd is None:
        sys.exit(1)

    print(f"{total_files - remain_files_in_releases} files to handle")

    for _, record_url in mp_pd.iterrows():
        release_id = record_url["release_id"]
        file_exists = os.path.join(releases, f"{release_id}.json")
        if not os.path.exists(file_exists):
            th = threading.Thread(
                target=thr, name=release_id, args=(record_url["release_uri"],)
            )
            th.start()
            th.join()


if __name__ == "__main__":
    run()
