"""
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

This code is a script to get Discogs data and persists it on a CSV file.
TODO: offer any way to store internally data (CSV, SQLITE, PGSQL, MONGO...)
"""
from collections import Counter
from datetime import datetime
import glob
from itertools import chain
from multiprocessing.pool import ThreadPool as Pool
import os

import dotenv
import pandas as pd
from pandas.util.testing import assert_frame_equal

from app.utils.configuration import MAIN_URL, USERNAME
from app.utils.discogs_covers import get_cover
from app.utils.discogs_requests import get
from app.utils.files import slugit, DB, get_filename
from app.utils.logger import LOGGER

# MAIN_URL = 'https://api.discogs.com'

DISCOGS_LINK = "https://www.discogs.com/{}/release/{}"

dotenv_path = os.path.join(os.path.dirname(__file__), ".env")
dotenv.load_dotenv(dotenv_path)

user_agent = "discogs_api_client/2.0"

# username = 'lezef'

access_token = os.environ.get("DISCOGS_CURRENT_TOKEN")

now = datetime.now().strftime("%Y%m%d-%H%M")

prices = []

listing = []


def inventory(*args, **kwargs):
    """
    Returns Discogs Sales Objects from the website API.
    :param args: arguments
    :param kwargs: keyword arguments
    :return: string|JSON object from API
    """
    sort_collection = [
        "listed",
        "price",
        "item",
        "artist",
        "label",
        "catno",
        "audio",
        "status",
        "location",
    ]

    if kwargs:
        if "status" in kwargs.keys():
            value = kwargs["status"]
            kwargs["status"] = value.replace(" ", "+")
        if "sort" in kwargs.keys():
            if kwargs["sort"] not in sort_collection:
                del kwargs["sort"]
        req = [f"{k}={v}" for k, v in kwargs.items()]
        final_req = "&".join(req)

    if final_req:
        args = f"?{final_req}"
    else:
        args = ""

    sales_endpoint = f"{MAIN_URL}/users/{USERNAME}/inventory{args}"
    return get(sales_endpoint)


def marketplaceCollector(item):
    """
    Refactors (serializes) data from Discogs item to a better human-readable way.
    :param item: dict
    :return: dict
    """
    global prices
    excluded_keys = [
        "original_shipping_price",
        "seller",
        "shipping_price",
        "original_price",
        "ships_from",
        "uri",
        "audio",
        "posted",
        "in_cart",
        "status",
    ]

    replace_keys = {
        "id": "listing_id",
        "external_id": "location",
        "condition": "media_condition",
        "posted": "listed",
    }

    item["accept_offer"] = "N" if item["allow_offers"] is False else "Y"
    del item["allow_offers"]

    for del_key in excluded_keys:
        del item[del_key]

    for k, v in replace_keys.items():
        if k in item.keys():
            item[v] = item[k]
            del item[k]

    item["price"] = item["price"]["value"]
    item["artist"] = item["release"]["artist"]
    item["title"] = item["release"]["title"]
    item["description"] = item["release"]["description"]
    item["format"] = item["release"]["format"]
    item["release_id"] = item["release"]["id"]
    item["release_uri"] = item["release"]["resource_url"]
    item["release_year"] = item["release"]["year"]
    item["discogs_link"] = DISCOGS_LINK.format(
        slugit(item["title"]), item["release_id"]
    )
    del item["release"]
    prices.append(item["price"])

    cover_txt = item["artist"].upper() + ": " + item["title"]

    get_cover(item["release_uri"], item["release_id"], release_txt=cover_txt)

    return item


def all_sales():
    """
    Gets all data from Discogs API Marketplace, avoiding
    page limitation.
    :return: list
    """
    try:
        for_sale = inventory(status="For Sale", per_page=100)
        # max_items = for_sale['pagination']['items']
        pages = for_sale["pagination"]["pages"]

        ind = 1
        sales = []

        while ind < pages:
            sales.append(for_sale["listings"])
            ind += 1

            if ind < pages:
                tmp = inventory(
                    status="For Sale",
                    per_page=100,
                    page=for_sale["pagination"]["urls"]["next"],
                )
            else:
                tmp = []
                sales.append(get(for_sale["pagination"]["urls"]["last"])["listings"])
                break
            for_sale = tmp
            tmp = []

        return list(chain.from_iterable(sales))
    except KeyError as err:
        LOGGER.warning(f"Connection refused. Re-try it later. Error message: {err}")


def multithreading_pool():
    """
    Multithreading Management System, to handle each Discogs object
    into our serializer (marketplaceCollecto).
    """
    global listing
    pool = Pool(2)
    for elem in all_sales():
        item = pool.apply_async(marketplaceCollector, (elem,))
        listing.append(item.get())
    pool.close()
    pool.join()


if __name__ == "__main__":
    os.chdir(DB)

    this_file = get_filename(__file__)

    multithreading_pool()

    df1 = pd.DataFrame(listing)
    df_prices = pd.DataFrame(prices)

    current_csv_file = glob.glob("./*.csv")

    if not current_csv_file:
        csv_filename = f"discogs_sales_{now}.csv"
        df1.to_csv(csv_filename, index=False)
    else:
        current_csv_file = current_csv_file[0]
        # Important: avoid NaN values where no values in columns
        # else no more comparison is possible!
        df2 = pd.read_csv(current_csv_file, na_filter=False)

    try:
        assert_frame_equal(df1, df2)

        LOGGER.info(f"[{this_file}] Same data in both ways. Nothing to do.")

    except:
        LOGGER.info(
            f"[{this_file}] Not the same data frame. Need to overwrite the new one..."
        )

        os.remove(current_csv_file)
        csv_filename = f"discogs_sales_{now}.csv"
        df1.to_csv(csv_filename, index=False)
        LOGGER.info(f"[{this_file}] New CSV file written successfully.")
    finally:
        """
        Many triggers can be setting up here: global price, quantity, changes
        inside the Counter object, etc.
        """
        LOGGER.info(f"[{this_file}] Discogs Collection Prices Information")

        LOGGER.info(sorted(Counter(prices).items()))

        LOGGER.info(df_prices.describe())
