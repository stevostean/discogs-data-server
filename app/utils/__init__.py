import os

import dotenv

APP_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
DOTENV_PATH = os.path.join(APP_ROOT, ".env")
dotenv.load_dotenv(DOTENV_PATH)

CONSUMER_KEY = os.environ.get("DISCOGS_CONSUMER_KEY")
CONSUMER_SECRET = os.environ.get("DISCOGS_CONSUMER_SECRET")

OAUTH_KEY = os.environ.get("DISCOGS_OAUTH_KEY")
OAUTH_SECRET = os.environ.get("DISCOGS_OAUTH_SECRET")

OAUTH_TOKEN = os.environ.get("DISCOGS_CURRENT_TOKEN")

USER_AGENT = "studiomzk/0.1 +https://studiomzk.com"

HEADERS = {
    "Authorization": f"Discogs token={OAUTH_TOKEN}",
    "User-Agent": USER_AGENT,
    "Accept-Encoding": "gzip, deflate",
}

__all__ = [
    "converters",
    "discogs_covers",
    "discogs_requests",
    "files",
    "images",
    "logger",
    "misc",
    "network",
]
