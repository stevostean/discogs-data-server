import configparser
from multiprocessing import cpu_count
import os

from app.utils import APP_ROOT as ROOT

_CONFIG = configparser.ConfigParser()
_CONFIG.read(os.path.join(ROOT, "config.ini"))

DISCOGS = _CONFIG["Discogs"]
USERNAME = DISCOGS["USERNAME"]
MAIN_URL = DISCOGS["MAIN_URL"]
LOGPATH = DISCOGS["LOGPATH"]
WORKERS = cpu_count() - 1

if __name__ == "__main__":
    print(_CONFIG)
