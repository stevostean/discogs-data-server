"""
Utils/converters Module.
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

This module contains main converters for data formatlike CSV, JSON, etc.
TODO: create one big function to convert in both way.
It could be useful to not repeat the same process.
"""
import csv
import json
import os

from app.utils.files import default_destination
from app.utils.logger import LOGGER
from app.utils.misc import toNumber


def jsonToCSV(json_path, filepath=None):
    """
    Converts on-the-fly any JSON file to a CSV one
    TODO: a function to convert any CSV, XLS, XLSX format (argument)?
    TODO: argument for delimiter options & tar/gzip/zip compression?
    :param json_path: define absolute path to the JSON file to convert
    :param filepath: string|abspath+filename+extension (.csv)
    :return: void
    """
    with open(json_path) as jsonFile:
        data_parsed = json.loads(jsonFile.read())

    header = data_parsed[0].keys()
    # values = [list(item.values()) for item in data_parsed]

    if filepath is None:
        filepath = default_destination(json_path)

    if not os.path.exists(filepath):
        with open(filepath, "w", newline="") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=header)
            writer.writeheader()
            writer.writerows(data_parsed)

        LOGGER.info(f"New csv file: {filepath}")


def csvToJSON(csv_path, filepath=None):
    """
    Converts CSV file to JSON file format, with numeric auto-detection for values
    :param csv_path: string|abspath+filename+extension (.csv)
    :param filepath: string|abspath+filename+extension (.json)/default: csv_path
    :return: void
    """
    with open(csv_path, "r") as csvfile:
        reader = csv.reader(csvfile)
        headers = next(reader)
        # rows = [row for row in reader]
        rows = [[toNumber(rr) for rr in r] for r in reader]

    final_obj = [dict(zip(headers, row)) for row in rows]
    json_obj = json.dumps(final_obj, indent=2)

    if filepath is None:
        filepath = default_destination(csv_path)

    with open(filepath, "w") as jsonfile:
        jsonfile.write(json_obj)

    LOGGER.info(f"New JSON file: {filepath}")
