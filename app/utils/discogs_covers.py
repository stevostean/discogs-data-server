import os

from app.utils.files import COVERS, check_or_create_directory
from app.utils.discogs_requests import get, download


def get_cover(release_uri, release_id, path=None, release_txt=None):
    img_path = check_or_create_directory(COVERS)
    if path is not None:
        img_path = os.path.join(COVERS, path)
        if not os.path.exists(img_path):
            check_or_create_directory(img_path)
    img_filename = f"{release_id}.jpg"
    final_path = os.path.join(img_path, img_filename)

    if os.path.exists(final_path):
        return img_filename
    res_data = get(release_uri)

    try:
        img_link = res_data["images"][0]["uri"]
    except KeyError:
        # img_link = create_cover(f'{release_txt}', img_path, file_name=release_id)
        # return img_filename
        img_link = None

    if img_link is not None:
        download(img_link, path=final_path)
        return img_filename

    return False
