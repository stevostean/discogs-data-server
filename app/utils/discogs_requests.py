"""
Utils/discogs_requests Module.
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

This module contains all specific web requests to discogs.com
with DJ Zef credentials for his own purpose (create a javascript front-end
website, displaying its records he is selling).

"""
from random import random
from time import sleep
from urllib.parse import quote_plus

import requests

from app.utils import APP_ROOT, HEADERS

ROOT = APP_ROOT

MAIN_URL = "https://api.discogs.com"


class DiscogsError(Exception):
    """Specific exception for all the functions here."""

    def __init__(self, message, errors=None):
        super(DiscogsError, self).__init__(message)
        self.errors = errors


def get(url):
    """
    Send a request to website to get JSON response.
    :param url: string|URI
    :return: string|Json/Exception
    """
    try:
        response = requests.get(url, headers=HEADERS)
        return response.json()
    except Exception as err_msg:
        raise DiscogsError(err_msg)


def download(url, path=None):
    """
    Request to download image files from Discogs (with
    limitations).
    :param url: string
    :param path: string|Path to put file
    :return: bool/Exception
    """
    try:
        req = requests.get(url, headers=HEADERS, stream=True)
        if req.status_code == 200:
            req.raw.decode_content = True
            with open(path, "wb") as local_file:
                for chunk in req:
                    local_file.write(chunk)
        # more than 1 second to wait because of API's request limitations.
        sleep(random() + 1)
        return True
    except Exception as err_msg:
        raise DiscogsError(err_msg)


def post(url):
    """
    Send a post request to get JSON data.
    :param url: string
    :return: string|JSON/Exception
    """
    try:
        response = requests.post(url, headers=HEADERS)
        return response.json()
    except Exception as err_msg:
        raise DiscogsError(err_msg)


def search(query=""):
    """
    Send a Search Specific request on Discogs to get JSON data.
    :param query: string
    :return: string|JSON/Exception
    """
    encoded_query = quote_plus(query)
    url = f"https://api.discogs.com/database/search?q={encoded_query}"

    try:
        req = requests.get(url, headers=HEADERS)
        return req.json()
    except Exception as err:
        raise DiscogsError(err)


if __name__ == "__main__":
    MYSELF = get("https://api.discogs.com/marketplace/listings/387227011")

    print(MYSELF)
