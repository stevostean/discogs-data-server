# -*- coding: utf-8 -*-
"""Files Module for Discogs App.

This module demonstrates documentation as specified by the `Files Module \
for Discogs App`_.
Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python utils/files.py
        $ python -m doctest -v utils/files.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * For module TODOs
    * You have to also use ``sphinx.ext.todo`` extension

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
import os
import re

from app.utils.discogs_requests import ROOT


MAIN_DIR = os.path.join(ROOT, "files")
RELEASES = os.path.join(MAIN_DIR, "releases")
JSON_FILES = os.path.join(MAIN_DIR, "json")
COVERS = os.path.join(MAIN_DIR, "images")
DB = os.path.join(MAIN_DIR, "db")
CSV_FILES = os.path.join(MAIN_DIR, "csv")
LOGS = os.path.join(ROOT, "log")


def slugit(sentence, sign="-", lower=False):
    """
    Returns any string sentence to a strict HTML path (lowercase, no space)
    :param sentence: string
    :param sign: string
    :param lower: bool
    :return: string

    Some big testing suite here, to fine tune this slugify function
    >>> slugit("à la fête à neuneu")
    'a-la-fete-a-neuneu'

    >>> st = "Etre né quelque part, pour celui qui est né, c'est toujours un hasard ?"
    >>> slugit(st, lower=True)
    'etre-ne-quelque-part-pour-celui-qui-est-ne-cest-toujours-un-hasard'
    """
    translation = str.maketrans("éàèùâêîïöôûüçöë", "eaeuaeiioouucoe")
    blank_char = re.sub(r"\,|\.|\;|\:", " ", sentence)
    none_char = re.sub(r"\"|\'|\!|\?|\(|\)", "", blank_char)

    output = none_char.translate(translation)

    if lower:
        output = output.lower()

    out = list(filter(None, output.split(" ")))
    output = sign.join(out)

    return output


def check_or_create_directory(path, start=MAIN_DIR):
    """
    Check if directory exists or create it, according to the path and potential
    absolute root path.
    :param path: string|path
    :param start: string|path
    :return: string|path

    Just checking if this function runs well on my local machine (it has to be
    re-written on other systems) or just SKIP it #doctest: +SKIP
    >>> check_or_create_directory('releases')
    '/home/me/Documents/Codes/python/discogs_2018/files/releases'
    """
    abs_path = os.path.join(start, path)
    if abs_path in globals().values():
        if not os.path.exists(abs_path):
            os.makedirs(abs_path)
        return abs_path

    # os.makedirs(abs_path)
    return abs_path


def get_file_startswith(path, start=None, full=False):
    """
    Returns a filename based on its starting string search.
    :param path: string|path
    :param start: string|filename
    :param full: bool
    :return: bool|complete path with filename

    Testing if a file containing those starting string is on this directory
    >>> get_file_startswith(ROOT, start='TEST')
    'TEST_FILE'

    Testing the opposite
    >>> get_file_startswith(MAIN_DIR, start='nawak')
    False
    """
    if start is None:
        return False
    try:
        filelist = os.listdir(path)
    except FileNotFoundError:
        return False
    try:
        get_file = [filename for filename in filelist if filename.startswith(start)]
        if full:
            return os.path.join(path, get_file[0])
        return get_file[0]
    except FileNotFoundError:
        return False
    except IndexError:
        return False


def overwrite_file(filepath, content):
    """
    Re-write any specific file, based on its content.
    :param filepath: string|path
    :param content: any
    :return: bool

    Testing the function on a dummy file
    >>> overwrite_file(os.path.join(ROOT, 'TEST_FILE'), "Some content")
    True
    >>> file = open(os.path.join(ROOT, 'TEST_FILE'))
    >>> print(file.read())
    Some content
    """
    checking = os.path.exists(filepath)
    if checking:
        os.remove(filepath)
        with open(filepath, "w") as localfile:
            localfile.write(content)
        return True
    return False


def get_filename(scriptname):
    """
    Returns filename based on __file__ from script, without any extension.
    :param f: __file__
    :return: string

    Checking if this function works fine
    >>> get_filename(__file__)
    'files'
    """
    return os.path.basename(scriptname).strip(".py")


if __name__ == "__main__":
    FILES = check_or_create_directory("releases")
    print(FILES)

    NEW_SLUG = slugit("Si c'est ça a lieu à Pointe-à-Pître, c'est inouï ?", lower=True)
    print(NEW_SLUG)


def default_destination(path):
    """
    Save a file to the same place as source
    :param path: absolute path of the source file
    :return: absolute destination file
    """
    default = os.path.splitext(path)
    default_abs_filepath = default[0]
    default_filepath = default_abs_filepath.split("/")
    default_filepath.pop(0)
    filename = default_filepath.pop() + ".csv"
    final_path = "/".join(default_filepath)
    final_filepath = f"/{final_path}/{filename}"
    return final_filepath
