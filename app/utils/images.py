"""
Utils/images Module.
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

This module contains all specific functions to handle image file
from Discogs website. Using PIL library...

"""
from random import randrange

from PIL import Image, ImageDraw, ImageFont


def get_size(img_path):
    """
    Return the size in pixel of image file.
    :param img_path: string|path
    :return: tuple|(width,height) - in pixels
    """
    image = Image.open(img_path)
    return image.size


def create_cover(txt, file_path, file_name):
    """
    Create generic thumbnails with one background
    color & Artists + Title infos on it.
    :param txt: string|infos about this record
    :param file_path: string|filepath
    :param file_name: string|filename
    :return: string|filepath+filename+jpg extension
    """
    img_size = (500, 500)
    font_size = 14
    font = "/usr/share/fonts/TTF/DejaVuSansMono.ttf"
    fnt = ImageFont.truetype(font=font, size=font_size)
    rgb_colors = (randrange(0, 100), randrange(0, 100), randrange(0, 100))

    image = Image.new("RGB", size=img_size, color=rgb_colors)
    draw = ImageDraw.Draw(image)
    y_pos = (img_size[1] / 2) - (font_size / 2)
    draw.text((10, y_pos), txt, font=fnt, fill=(255, 255, 255))
    nu_file_path = f"{file_path}/{file_name}.jpg"
    image.save(nu_file_path, "JPEG")
    # print(file_name)
    return nu_file_path
