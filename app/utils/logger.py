"""
Utils/logger Module.
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

A logging system available via LOGGER variable, when module is imported.
It is possible to disable some parts of it, like this:
    from utils.logger import LOGGER, STREAM, FILE_HANDLER

    LOGGER.removeHandler(STREAM) # prevent any console output
    LOGGER.removeHandler(FILE_HANDLER) # prevent any entry in logger file
 """
import logging
from logging.handlers import RotatingFileHandler
import os, sys

sys.path.append("..")

from app.utils.configuration import LOGPATH
from app.utils.files import LOGS, check_or_create_directory

logpath = check_or_create_directory(LOGPATH)
to_logfile = os.path.join(logpath, "discogs.log")
if not os.path.exists(to_logfile):
    os.mknod(to_logfile)
LOGFILE = to_logfile

# LOGFILE = os.path.join(logpath, 'discogs.log')

LOG_LEVEL = logging.DEBUG

LOGGER = logging.getLogger()
LOGGER.setLevel(LOG_LEVEL)

FORMATTER = logging.Formatter(
    "%(asctime)s - %(levelname)s: %(message)s", datefmt="%d/%m/%Y %H:%M:%S"
)

STREAM = logging.StreamHandler()
STREAM.setLevel(LOG_LEVEL)
LOGGER.addHandler(STREAM)


def logfile():
    """
    Creates an empty file if file is not at its filepath.
    :return: string|filepath with filename and its extension
    """
    if not os.path.exists(LOGFILE):
        os.mknod(LOGFILE)  # create empty file

    return LOGFILE


# création d'un handler qui va rediriger une écriture du log vers
# un fichier en mode 'append', avec 10 backups et une taille max de 1Mo
FILE_HANDLER = RotatingFileHandler(logfile(), "a", 1000000, 10)
FILE_HANDLER.setLevel(logging.DEBUG)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

# Remove all logs from requests (due to urllib3)
REQUESTS = logging.getLogger("urllib3")
REQUESTS.setLevel(logging.CRITICAL)
LOGGER.removeHandler(REQUESTS)

if __name__ == "__main__":
    LOGGER.info("Just an update")
    LOGGER.warning("Bad warning: %s", "404")
