"""
Utils/misc Module.
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

Everything else... Just some helpers.
"""

from functools import wraps
from time import time

from logger import LOGGER, FILE_HANDLER
from files import ROOT


LOGGER.removeHandler(FILE_HANDLER)

final = 0


def timeit(func):
    """
    Decorator to get some benchmarks.
    :param func: function
    :return: LOGGER|function
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        """
        Handles the time elapsed between start and end time.
        :param args: arguments
        :param kwargs: keyword arguments
        :return: function execution handler
        """
        global final
        start = time()
        result = func(*args, **kwargs)
        final = time() - start
        return result

    LOGGER.info(f"ELAPSED TIME: {final} sec.")
    return wrapper


def toNumber(s):
    """
    simple numeric converters for values in strict string format
    :param s: string
    :return: int | float | string

    >>> toNumber('002')
    2
    >>> toNumber('0.2354')
    0.2354
    >>> toNumber('OSS117')
    'OSS117'
    """
    # print(type(s))
    try:
        return int(s)
    except:
        try:
            return float(s)
        except:
            return s


def root():
    """
    Just return ROOT path.
    :return: string|ROOT

    >>> root()
    '/home/me/Documents/Codes/python/discogs_2018'
    """
    return ROOT


if __name__ == "__main__":
    print(root())
