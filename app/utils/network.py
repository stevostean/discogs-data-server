"""
Utils/network Module.
Created by Stéphane ESTEVE, MEZKLADOR, studiomzk.com, Lyon, FRANCE, May 2018.

Few tools to check network connectivity. Useless?
"""
import requests


class WebsiteDownException(Exception):
    """Customized exception for this module."""

    pass


def ping_website(address, timeout=20, headers=None):
    """
    Checks if website is down at the URI argument.
    :param address: string|URI
    :param timeout: integer|seconds
    :param headers: dict|key/value for a specific request
    """
    try:
        response = requests.head(address, timeout=timeout, headers=headers)
        if response.status_code >= 400:
            raise WebsiteDownException(f"Bad status code: {response.status_code}")
    except requests.exceptions.RequestException:
        raise WebsiteDownException(f"The server at {address} is not reachable.")

    return True


def check_website(address):
    """
    Just a simple usecase of the functionbelow.
    :param address: string|URI
    """
    try:
        ping_website(address)
    except WebsiteDownException:
        pass
