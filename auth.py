import discogs_client
from discogs_client.exceptions import HTTPError
import dotenv
import os
from pprint import pprint
import sys
import time


def browse_artists(artists):
    artists_list_len = len(artists)
    if len(artists) < 1:
        artist = artists[0]
        return artist.name
    else:
        return ", ".join([artist.name for artist in artists])


start = time.time()
dotenv_path = os.path.join(os.path.dirname(__file__), ".env")
dotenv.load_dotenv(dotenv_path)

consumer_key = os.environ.get("DISCOGS_CONSUMER_KEY")
consumer_secret = os.environ.get("DISCOGS_CONSUMER_SECRET")

try:
    oauth_token = os.environ.get("DISCOGS_OAUTH_KEY")
    oauth_secret = os.environ.get("DISCOGS_OAUTH_SECRET")
except KeyError:
    oauth_token = None
    oauth_secret = None

user_agent = "discogs_api_client/2.0"

dc = discogs_client.Client(user_agent)
dc.set_consumer_key(consumer_key, consumer_secret)

if oauth_token == None and oauth_secret == None:

    token, secret, url = dc.get_authorize_url()

    out = (
        " ==> Request Token ",
        f"    * OAUTH_TOKEN           = {token}",
        f"    * oauth_token_secret    = {secret}",
        f"Please, browse to the following url {url}",
    )
    out = "\n".join(map(str, out))
    print(out)

    accepted = "n"
    while accepted.lower() == "n":
        print()
        accepted = input("Have you authorized me at {} [y/n] :".format(url))

    oauth_verifier = input("verification code :")

    try:
        oauth_token, oauth_secret = dc.get_access_token(oauth_verifier)
        dotenv_nu_content = """
        DISCOGS_OAUTH_KEY=\"{}\"
        DISCOGS_OAUTH_SECRET=\"{}\"
        """.format(
            oauth_token, oauth_secret
        )
        with open(dotenv_path, "a") as f:
            f.write(dotenv_nu_content)
        # os.environ['DISCOGS_OAUTH_KEY'] = OAUTH_TOKEN
        # os.environ['DISCOGS_OAUTH_SECRET'] = OAUTH_SECRET
    except HTTPError:
        print("Unable to authenticate")
        sys.exit(1)

else:
    dc.set_token(oauth_token, oauth_secret)

user = dc.identity()

out = (
    " === User Informations === ",
    "    * Username = {}".format(user.username),
    "    * Name = {}".format(user.name),
    " === Access Token === ",
    "    * OAUTH_TOKEN = {}".format(oauth_token),
    "    * oauth_token_secret = {}".format(oauth_secret),
    "Future requests will be signed with the above tokens.\n",
)
# Concatenate strings in tuple
out = "\n".join(map(str, out))
print(out)

"""
Getting items from marketplace / inventory
"""
marketplace = user.inventory
mp_total_pages = marketplace.pages
print(mp_total_pages)
big_mp_list = []
for p in range(0, mp_total_pages):
    mp_page = marketplace.page(p)
    for it in mp_page:
        if it.status == "For Sale":
            big_mp_list.append(it.release)
mp_products = [mp_p.id for mp_p in big_mp_list]
print("Total of records to sell: {}".format(len(big_mp_list)))

"""
Getting all items from ALL folder collection
"""
all_folder = user.collection_folders[0]
print(user.collection_folders)
final_count = int(all_folder.data["count"])
# print("total of records: {}".format(final_count))
print("All folder info: {}".format(vars(all_folder)))
all_releases = all_folder.releases
all_releases._per_page = final_count
# all_release_class = all_releases.class_
print("All releases: {}".format(vars(all_releases)))

"""
Just checking if some items from ALL are for Sale (colision with inventory)
"""
for f in all_releases.page(0):
    release = f.release
    if release.id in mp_products:
        print(f"{release.title} with id {release.id} is for sale.")

# time.sleep(10)
exit()

"""
Gettings infos from each item
"""
list_of_folders = []
for folder in all_releases.page(0):
    release = folder.release
    list_of_folders.append(release)
    print(f"ID={release.id}")
    artists = release.artists
    artist = []
    for art in artists:
        artist.append(art.name)
    print("Artists={}".format(", ".join(artist)))
    print(f"title={release.title}")
    labels_name = ", ".join([l.name for l in release.labels])
    # catnos = ", ".join([l.catno for l in release.labels])
    print(f"Labels: {labels_name} / ")
    print(f"format={release.formats}")
    print(f"Thumb={release.thumb}")
    print("=" * 20)


first_release = list_of_folders[-1]
print("Your last object: {}".format(first_release))
print("Properties: {}\n\n".format(first_release.__dict__))
some_infos = (
    "Some infos:",
    f"id={first_release.id}",
    f"title={first_release.title}",
    f"format={first_release.formats[0]}",
)
print(some_infos.join("\n"))
artists = browse_artists(first_release.artists)
print(f"Artists: {artists}")
pprint(f"Images: {first_release.images}")

final = "it takes {} sec to execute all that script.".format(time.time() - start)
print("=" * len(final))
print(final)
