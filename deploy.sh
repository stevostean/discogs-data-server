#!/usr/bin/bash

deploy_path=$(awk -F "=" '/DEPLOY_TO/ {print $2}' config.ini)

rsync -avz $PWD/files/* $deploy_path
