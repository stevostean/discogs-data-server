"""Main execution script for automation."""
import argparse
import os
import subprocess
import sys


PYTHON_EXEC = os.path.join(os.path.dirname(__file__), "venv/bin/python")


def main(verbosity=False, deploy=True):
    """main script to run with Arguments."""
    p1 = subprocess.Popen(
        [PYTHON_EXEC, "main.py"], stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    out1, err1 = p1.communicate()

    out2 = err2 = None

    if deploy:
        p2 = subprocess.Popen(
            ["/usr/bin/bash", "deploy.sh"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        out2, err2 = p2.communicate()

    if not verbosity:
        out1 = err1 = out2 = err2 = None

    return out1, err1, out2, err2


if __name__ == "__main__":
    val = True
    parser = argparse.ArgumentParser()
    """
    With -v / --verbose argument, it's possible to display subprocesses
    outputs in terminal.
    """
    parser.add_argument(
        "--verbose",
        "-v",
        help="Display any messages from subprocesses.",
        action="store_true",
    )
    # Inverted const state in an Argument, made possible by:
    # https://stackoverflow.com/questions/50008913/how-to-toggle-a-boolean-argparse-option#answer-50009888
    parser.add_argument(
        "--no-deployment",
        "--no-deploy",
        "-N",
        help="Avoid any deployment, as intended by default.",
        action="store_const",
        const=not (val),
        default=val,
    )
    args = parser.parse_args()

    o1, e1, o2, e2 = main(verbosity=args.verbose, deploy=args.no_deployment)
    if not o1 == e1 == None:
        print(f"Python Threads: \n{o1.decode()}, {e1.decode()}")
        if not o2 == e2 == None:
            print(f"Deployment: \n{o2.decode()}, {e2.decode()}")
    sys.exit(0)
