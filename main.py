"""Threads launcher for different parts of the Discogs Server."""
from collections import deque
import inspect
import queue
import threading

from app.processes.initialization import run as INIT
from app.processes.converts import run as CONVERTS
from app.processes.releases import run as RELEASES
from app.processes.genres import run as GENRES

SCRIPTS = deque([INIT, CONVERTS, GENRES, RELEASES])

if __name__ == "__main__":
    q = queue.Queue()

    p1 = SCRIPTS.popleft()
    p1_name = inspect.getfile(p1).split("/")[-1].replace(".py", "").upper()
    p1_thread = threading.Thread(target=p1, name=p1_name, args=())
    p1_thread.start()
    p1_thread.join()

    if not p1_thread.is_alive():
        while True:
            try:
                func = SCRIPTS.popleft()
                fn = inspect.getfile(func).split("/")[-1].replace(".py", "").upper()
                print(f"Function: {fn}")
                p = threading.Thread(target=func, name=fn, args=())
                p.start()
                q.put(p.join(timeout=1500))
            except IndexError:
                break
