import os

from processes.utils.files import (
    JSON_FILES,
    CSV_FILES,
)
from processes.utils.converters import (
    jsonToCSV,
)


if __name__ == '__main__':
    # FIRST: convert JSON to CSV
    lowcosts = os.path.join(JSON_FILES, 'lowcosts.json')
    jsonToCSV(lowcosts, os.path.join(CSV_FILES, 'lowcosts.csv'))

    bankables = os.path.join(JSON_FILES, 'bankables.json')
    jsonToCSV(bankables, os.path.join(CSV_FILES, 'bankables.csv'))

    # THEN: convert csv to JSON
    #lowcosts = os.path.join(CSV_FILES, 'lowcosts.csv')
    #csvToJSON(lowcosts, os.path.join(JSON_FILES, 'converted_lowcosts.json'))

